import React, { Component } from 'react';
import './style.css';

class ContactCard extends React.Component {
  render() {
    const contact = this.props.contact;

    return (
      <li>
        <div className="card">
          <img className="card-img" src={contact.avatar} alt="avatar"></img>
          <div className="card-detail">
            <label className="block card-header-text">{contact.first_name},</label>
            <label className="block card-header-text">{contact.last_name}</label>
            <label className="block card-text">ID: {contact.id}</label>
          </div>
        </div>
      </li>
    );
  }
}

export default ContactCard;